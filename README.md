# Presentation on sapper

## Presentation tool Ideas

- [x] Integrate mdsvex
- [x] Make slidelike software
- [x] Nested routes using slots and routes
- [ ] Mobile controller (using webrtc) inc notes
- [ ] Route transitions

## Talk todo's 

- [ ] Structure it
- [ ] Design setup
- [ ] Slide store? for route transitions, sub-routes and on-page state

## Questions:

- (Does it scale?)[https://github.com/sveltejs/svelte/issues/2546]