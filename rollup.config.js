import { mdsvex } from 'mdsvex'
import path from 'path'

import alias from '@rollup/plugin-alias';
import replace from '@rollup/plugin-replace';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import yaml from '@rollup/plugin-yaml';
import image from '@rollup/plugin-image';

import svelte from 'rollup-plugin-svelte';
import babel from 'rollup-plugin-babel';
import { terser } from 'rollup-plugin-terser';
import config from 'sapper/config/rollup.js';
import pkg from './package.json';

import hljs from 'highlight.js'

const mode = process.env.NODE_ENV;
const dev = mode === 'development';
const legacy = !!process.env.SAPPER_LEGACY_BUILD;
const aliased = {
  entries: [
    { find: /^src\//, replacement: `${__dirname}/src/` },
    { find: /^content\//, replacement: `${__dirname}/content/` },
  ]
}
const highlight = (str, lang) => {
  if (lang && hljs.getLanguage(lang)) {
    try {
      return '<pre class="hljs"><code>' +
             hljs.highlight(lang, str, true).value +
             '</code></pre>';
    } catch (__) {}
  }

  return ''; // use external default escaping
}

const onwarn = (warning, onwarn) => (warning.code === 'CIRCULAR_DEPENDENCY' && /[/\\]@sapper[/\\]/.test(warning.message)) || onwarn(warning);
const dedupe = importee => importee === 'svelte' || importee.startsWith('svelte/');

export default {
  client: {
    input: config.client.input(),
    output: config.client.output(),
    plugins: [
      alias(aliased),
      replace({
        'process.browser': true,
        'process.env.NODE_ENV': JSON.stringify(mode)
      }),
      image(),
      svelte({
        extensions: ['.svelte', '.svexy'],
        preprocess: mdsvex({
          // layout: path.join(__dirname, './src/DefaultLayout.svelte'), // this needs to be an absolute path
          // parser: md => md.use(SomePlugin), // you can add markdown-it plugins if the feeling takes you
          // you can add markdown-it options here, html is always true
          markdownOptions: {
            typographer: true,
            linkify: true,
            highlight,
          },
        }),
        dev,
        hydratable: true,
				emitCss: false
      }),
      resolve({
        browser: true,
        dedupe,
				extensions: ['.svelte', '.svexy', '.html', '.js', '.mjs', '.yaml', '.yml', '.css'],
      }),
      yaml(),
      commonjs(),

      legacy && babel({
        extensions: ['.js', '.mjs', '.html', '.svelte'],
        runtimeHelpers: true,
        exclude: ['node_modules/@babel/**'],
        presets: [
          ['@babel/preset-env', {
            targets: '> 0.25%, not dead'
          }]
        ],
        plugins: [
          '@babel/plugin-syntax-dynamic-import',
          ['@babel/plugin-transform-runtime', {
            useESModules: true
          }]
        ]
      }),

      !dev && terser({
        module: true
      })
    ],

    onwarn,
  },

  server: {
    input: config.server.input(),
    output: config.server.output(),
    plugins: [
      alias(aliased),
      replace({
        'process.browser': false,
        'process.env.NODE_ENV': JSON.stringify(mode)
      }),
      image(),
      svelte({
        extensions: ['.svelte', '.svexy'],
        generate: 'ssr',
        dev,
        preprocess: mdsvex({
          layout: path.join(__dirname, './src/DefaultLayout.svelte'), // this needs to be an absolute path
          // parser: md => md.use(SomePlugin), // you can add markdown-it plugins if the feeling takes you
          // you can add markdown-it options here, html is always true
          markdownOptions: {
            typographer: true,
            linkify: true,
            highlight,
          },
        }),
      }),
      resolve({
        dedupe,
				extensions: ['.svelte', '.svexy', '.html', '.js', '.mjs', '.yaml', '.yml', '.css'],
      }),
      yaml(),
      commonjs()
    ],
    external: Object.keys(pkg.dependencies).concat(
      require('module').builtinModules || Object.keys(process.binding('natives'))
    ),

    onwarn,
  },

  serviceworker: {
    input: config.serviceworker.input(),
    output: config.serviceworker.output(),
    plugins: [
      resolve(),
      replace({
        'process.browser': true,
        'process.env.NODE_ENV': JSON.stringify(mode)
      }),
      commonjs(),
      !dev && terser()
    ],

    onwarn,
  }
};
