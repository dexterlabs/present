
import { elasticOut } from 'svelte/easing';

export let visible;

export function whoosh(node, params) {
  const existingTransform = getComputedStyle(node).transform.replace('none', '');

  return {
    delay: params.delay || 0,
    duration: params.duration || 400,
    easing: params.easing || elasticOut,
    css: (t, u) => `transform: ${existingTransform} scale(${t})`
  };
}
