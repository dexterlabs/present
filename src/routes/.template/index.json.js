import { slidesForRequest } from 'src/util'

export function get (req, res) {
  const slides = JSON.stringify(slidesForRequest(req))
  res.writeHead(200, {
    'Content-Type': 'application/json'
  })
  res.end(slides)
}

