import fs from 'fs';

let lookup;
const titles = new Map();

export function get_examples() {
  lookup = new Map();

  return fs.readdirSync('content/examples/')
    .filter(file => file !== 'meta.json')
    .map(example_dir => {
      const slug = example_dir.replace(/^\d+-/, '');

      if (lookup.has(slug)) throw new Error(`Duplicate example slug "${slug}"`);
      lookup.set(slug, example_dir);

      const metadata = JSON.parse(fs.readFileSync(`content/examples/${example_dir}/meta.json`, 'utf-8'));
      titles.set(slug, metadata.title);

      return {
        slug,
        title: metadata.title
      };
    })
}

export function get_example(slug) {
  if (!lookup || !lookup.has(slug)) get_examples();

  const dir = lookup.get(slug);
  const title = titles.get(slug);

  if (!dir || !title) return null;

  const files = fs.readdirSync(`content/examples/${dir}`)
    .filter(name => name[0] !== '.' && name !== 'meta.json')
    .map(name => {
      return {
        name,
        source: fs.readFileSync(`content/examples/${dir}/${name}`, 'utf-8')
      };
    });

  return { title, files };
}
