import send from '@polka/send-type';
import { get_examples } from './_examples.js';

const cache = new Map();

export function get(req, res) {
  const examples = get_examples()
  console.log("Got examples:", examples)

  if (examples) {
    send(res, 200, examples);
  } else {
    send(res, 404, {
      error: 'not found'
    });
  }
}
