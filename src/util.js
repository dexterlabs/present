import path from 'path'
import glob from 'glob'

export function findSlides(location, extensions = ['.svexy', '.svelte', '.html']) {
  // Findall pages in the slides subdirectory
  return glob.sync(`${location}/**`)
    .map(slide => {
      const extension = path.extname(slide)
      const basename = path.basename(slide)
      const relative = path.relative(location, slide)

      // Should be slide extension
      if (!extensions.includes(extension)) return
      // Should not be ignored dir
      if (basename.startsWith('_') || relative.match(/\/_|^_/)) return

      const cleanname = path.basename(relative, extension)
      const isIndex = cleanname === 'index'

      return isIndex ? path.dirname(relative) : path.join(path.dirname(relative), cleanname)
    })
    .sort()
    .filter(name => !!name)
}

export function slidesForRequest(request) {
  return findSlides(`src/routes/${path.basename(request.path, '.json')}`)
}
