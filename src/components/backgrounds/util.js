import { readable, derived } from 'svelte/store'

export const time =readable(
  new Date().getTime(),
  set => {
    if (!process.browser) return

    let next
    const tick = () => {
      set(new Date().getTime())
      next = requestAnimationFrame(tick)
    }
    tick()
    return () => cancelAnimationFrame(next)
  }
)
export const sinoid = (frequency = 1) => derived(time, $time => Math.sin($time / (1000 / frequency)))
